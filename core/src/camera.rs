use cgmath::*;
use wgpu::util::DeviceExt;

use crate::node::bind_group::BindGroup;

#[rustfmt::skip]
pub const GL_TO_WGPU_MAT: Matrix4<f32> = Matrix4::new(
	1.0,	0.0,	0.0,	0.0,
	0.0,	1.0,	0.0,	0.0,
	0.0,	0.0,	0.5,	0.0,
	0.0,	0.0,	0.5,	1.0,
);

crate struct Camera {
	crate eye_point:    Point3<f32>,
	crate target_point: Point3<f32>,
	crate up:           Vector3<f32>,
	crate aspect:       f32,
	crate vfov:         f32,
	crate znear:        f32,
	crate zfar:         f32,
	crate view:         ViewProjection,
}

impl Default for Camera {
	fn default() -> Self {
		Self {
			// +Y is up, +Z is towards the viewer, +X is to the right
			// -Z being forwards is a little weird, but +Y up is convention so here we are
			eye_point:    (0., 0., 2.).into(),
			target_point: (0., 0., 0.).into(),
			up:           cgmath::Vector3::unit_y(),
			aspect:       1.,
			vfov:         90.,
			znear:        1.,
			zfar:         1000.,
			view:         ViewProjection::new(),
		}
	}
}

impl Camera {
	crate fn build_view_projection_matrix(&self) -> Matrix4<f32> {
		// I assume "rh" is for "right handed", and "lh" would be a "left handed" view
		let view = Matrix4::look_at_rh(self.eye_point, self.target_point, self.up);
		// How does this math work?
		let projection = perspective(Deg(self.vfov), self.aspect, self.znear, self.zfar);

		GL_TO_WGPU_MAT * projection * view
	}

	crate fn update(&mut self, control: &CameraController) {
		let mut forward: Vector3<f32> =
			(self.target_point.to_vec() - self.eye_point.to_vec()).normalize();
		let mut right = forward.cross(self.up);

		forward *= match control {
			CameraController {
				fore: true,
				back: false,
				..
			} => 1.,
			CameraController {
				fore: false,
				back: true,
				..
			} => -1.,
			_ => 0.,
		};

		right *= match control {
			CameraController {
				right: true,
				left: false,
				..
			} => 1.,
			CameraController {
				right: false,
				left: true,
				..
			} => -1.,
			_ => 0.,
		};

		let move_vec = forward + right;
		// Note that normalize() doesn't return anything useful, which is pretty stupid. It's only
		// used to normalize self, not to return a normalized vector. If you try to do somtehing
		// like
		// let x = y.normalize();
		// you get a vector full of NaNs, which isn't very fun to debug.
		move_vec.normalize();
		let speed = 0.1;
		self.eye_point += move_vec * speed;
		self.target_point += (move_vec * speed).into();
		let mut view = self.view;
		view.update(&self);
		self.view = view;
	}
}

impl BindGroup for Camera {
	fn bind_group_layout(&self, device: &wgpu::Device) -> wgpu::BindGroupLayout {
		device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			entries: &[wgpu::BindGroupLayoutEntry {
				binding:    0,
				visibility: wgpu::ShaderStage::VERTEX,
				ty:         wgpu::BindingType::Buffer {
					ty:                 wgpu::BufferBindingType::Uniform,
					has_dynamic_offset: false,
					min_binding_size:   None,
				},
				count:      None,
			}],
			label:   Some("camera bind group"),
		})
	}

	fn bind_group(
		&self,
		device: &wgpu::Device,
		layout: &wgpu::BindGroupLayout,
		buffer: Option<&wgpu::Buffer>,
	) -> wgpu::BindGroup {
		// ViewProjection creates a block for the uniform data to sit in. Required by
		// Vulkan GLSL.
		let mut view = self.view.clone();
		view.update(&self);
		device.create_bind_group(&wgpu::BindGroupDescriptor {
			layout,
			entries: &[wgpu::BindGroupEntry {
				binding:  0,
				// Buffer needs to exist for bind group to work, so unwrap is... bad, kinda
				// unecessary, but good enough for debug.
				resource: buffer.unwrap().as_entire_binding(),
			}],
			label: Some("camera bind group"),
		})
	}
}

crate struct CameraController {
	crate left:  bool,
	crate right: bool,
	crate fore:  bool,
	crate back:  bool,
	crate up:    bool,
	crate down:  bool,
}

impl Default for CameraController {
	fn default() -> Self {
		CameraController {
			left:  false,
			right: false,
			fore:  false,
			back:  false,
			up:    false,
			down:  false,
		}
	}
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
crate struct ViewProjection {
	matrix: [[f32; 4]; 4],
}

impl ViewProjection {
	crate fn new() -> Self {
		Self {
			matrix: cgmath::Matrix4::identity().into(),
		}
	}

	crate fn buffer(self, device: &wgpu::Device) -> wgpu::Buffer {
		device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label:    Some("view buffer"),
			contents: bytemuck::cast_slice(&[self]),
			usage:    wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
		})
	}

	crate fn update(&mut self, camera: &crate::camera::Camera) {
		self.matrix = camera.build_view_projection_matrix().into();
	}
}
