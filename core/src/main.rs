#![feature(crate_visibility_modifier)]
use futures::executor::block_on;
use winit::{
	event::*,
	event_loop::{
		ControlFlow,
		EventLoop,
	},
	window::WindowBuilder,
};

// Since I have no one place to write this comment, I'll put it here:
// BIND GROUPS
// Bind groups are how resources get passed into a shader. Bind group layouts are
// currently defined in crate::pipeline::Pipeline::new()
// Pipelines are kinda hard to mutate, so instead of sticking bind groups into pipelines
// we only stick in a generic bind group *layout*. That way, we can chuck any bind group
// we want into the encoder as long as it fits the bind group layout in the pipeline. That
// (in theory) makes texture swaps and stuff really simple and fast.
//
// Definitly worth noting that bind group =/= vulkan uniform buffer object =/= vulkan
// descriptor etc. It's really kind of its own thing. A bind group describes a `set` in
// the shader. A UBO, texture, sampler, whatever goes in one of the bindings in the bind
// group. That's why it's called a bind *group*: a set is a *group* of *bindings*. Duh.
//
// The textures and camera are each in separate bind group layouts. Technically, they
// don't have to be. But, they are logically distinct and visible in different shader
// stages. They could technically be all part of the same set/bind group, but they're not.

// Texture and vertex buffers
mod camera;
mod math;
// glTF data cleanup. TODO: build a glTF importer that's a bit simpler than the gltf crate.
mod material;
mod model_data;
mod node;
mod render;
mod vertex;

use node::{
	bind_group::BindGroup,
	Node,
};
use render::RenderState;

fn main() {
	simple_logger::SimpleLogger::new()
		.with_level(log::LevelFilter::Info)
		.init()
		.unwrap();

	let event_loop = EventLoop::new();
	let window = WindowBuilder::new()
		.with_inner_size(winit::dpi::PhysicalSize {
			width:  1024,
			height: 1024,
		})
		.build(&event_loop)
		.unwrap();

	let mut state = block_on(RenderState::new(&window));
	// Do NOT build this every frame. Loading textures every frame is dummy slow,
	// it WILL lock your computer.
	// In fact, don't build ANYTHING every frame. Don't rebuild gltf, don't rebuild bind groups,
	// don't rebuild pipelines, don't rebuild anything. It's bad for morale.
	// Investigate whether it's worth it to use split glTF instead of holding everything in one
	// file. Have .gltf, .bin, and texture files.
	let gltf = model_data::ModelData::new(&state, "../data/cube.gltf").unwrap();
	let mut camera = camera::Camera::default();
	camera.aspect =
		state.swapchain_descriptor.width as f32 / state.swapchain_descriptor.height as f32;
	let camera_buffer = camera.view.buffer(&state.device);
	let mut camera_controller = camera::CameraController::default();
	// include_bytes!() does some sort of byte alignment crap that wgpu apparently cares about.
	// Don't want to use include_spirv!() because I want to be able to pass in any shader to a
	// pipeline when I build it. Can't do that if I have to use string literals every time.
	// Also, all shaders are compiled in build.rs, so we only need to grab the spv files.
	// Look into glob, see if that does whatever the heck include bytes does without being obtuse
	let vert_data = include_bytes!("./shaders/shader.vert.spv");
	let frag_data = include_bytes!("./shaders/shader.frag.spv");
	let diffuse = &gltf.material;
	let diffuse_layout = diffuse.bind_group_layout(&state.device);
	let camera_layout = camera.bind_group_layout(&state.device);
	let layouts = vec![diffuse_layout, camera_layout];
	let mut albedo = Node::new(
		"albedo",
		&state.device,
		state.swapchain_descriptor.format,
		vert_data.to_vec(),
		frag_data.to_vec(),
		layouts,
	);
	albedo.add_bind_group(
		"diffuse",
		diffuse.bind_group(&state.device, &albedo.bind_group_layouts[0], None),
		None,
	);
	albedo.add_bind_group(
		"camera",
		camera.bind_group(
			&state.device,
			&albedo.bind_group_layouts[1],
			Some(&camera_buffer),
		),
		Some(camera_buffer),
	);

	let mut fps = fps_counter::FPSCounter::new();
	let mut ticks = 0.;
	let mut total_frames = 0.;
	event_loop.run(move |event, _, control_flow| match event {
		Event::RedrawRequested(_) => {
			state.update(&albedo.buffers["camera"], camera.view);
			camera.update(&camera_controller);
			total_frames += fps.tick() as f64;
			ticks += 1.;
			match state.render(&gltf, &albedo) {
				Ok(_) => {},
				Err(wgpu::SwapChainError::Lost) => state.resize(state.size),
				Err(wgpu::SwapChainError::OutOfMemory) => {
					log::info!("Average FPS: {}", total_frames / ticks);
					*control_flow = ControlFlow::Exit;
				},
				// Any other errors are probably going to be fixed by the next frame
				Err(e) => eprintln!("{:?}", e),
			}
		},
		Event::MainEventsCleared => {
			// "RedrawRequested will only trigger once, unless we manually request it"
			// Why does it need to be requested more than once?
			window.request_redraw();
		},
		Event::WindowEvent {
			ref event,
			window_id,
		} if window_id == window.id() => {
			// Check for input first before handling window events
			if !state.input(event, &mut camera_controller) {
				match event {
					WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
					WindowEvent::KeyboardInput {
						input, ..
					} => match input {
						KeyboardInput {
							state: ElementState::Pressed,
							virtual_keycode: Some(VirtualKeyCode::Escape),
							..
						} => *control_flow = ControlFlow::Exit,
						_ => {},
					},
					WindowEvent::Resized(size) => {
						log::info!("Resizing window to {:?}", size);
						state.resize(*size);
						camera.aspect = state.swapchain_descriptor.width as f32
							/ state.swapchain_descriptor.height as f32;
					},
					WindowEvent::ScaleFactorChanged {
						new_inner_size, ..
					} => {
						// new_inner_size is &&mut so we have to deref twice
						log::info!("Resizing window to {:?}", new_inner_size);
						state.resize(**new_inner_size);
						camera.aspect = state.swapchain_descriptor.width as f32
							/ state.swapchain_descriptor.height as f32;
					},
					_ => {},
				}
			}
		},
		_ => {},
	});
}
