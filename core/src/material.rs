use image::GenericImageView;

use crate::node::bind_group::BindGroup;

// This struct isn't strictly representing a buffer, but it kinda acts like one and I don't want to
// make a new file for it so here it goes
crate struct Material {
	crate sampler: wgpu::Sampler,
	crate texture: wgpu::Texture,
	crate view:    wgpu::TextureView,
}

impl Material {
	crate fn default(device: &wgpu::Device, queue: &wgpu::Queue) -> Self {
		let width = 1;
		let height = 1;
		let rgba = vec![255, 255, 255, 255];
		let size = wgpu::Extent3d {
			width,
			height,
			depth: 1,
		};
		let texture = device.create_texture(&wgpu::TextureDescriptor {
			size,
			mip_level_count: 1,
			sample_count: 1,
			dimension: wgpu::TextureDimension::D2,
			format: wgpu::TextureFormat::Rgba8UnormSrgb,
			usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
			label: Some("Default texture"),
		});
		let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
		let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
			address_mode_u: wgpu::AddressMode::ClampToEdge,
			address_mode_v: wgpu::AddressMode::ClampToEdge,
			address_mode_w: wgpu::AddressMode::ClampToEdge,
			mag_filter: wgpu::FilterMode::Nearest,
			min_filter: wgpu::FilterMode::Nearest,
			mipmap_filter: wgpu::FilterMode::Nearest,
			..Default::default()
		});

		queue.write_texture(
			wgpu::TextureCopyView {
				texture:   &texture,
				mip_level: 0,
				origin:    wgpu::Origin3d::ZERO,
			},
			rgba.as_slice(),
			wgpu::TextureDataLayout {
				offset:         0,
				bytes_per_row:  4 * width,
				rows_per_image: height,
			},
			size,
		);
		Self {
			sampler,
			texture,
			view,
		}
	}

	crate fn from_gltf(
		device: &wgpu::Device,
		queue: &wgpu::Queue,
		texture: gltf::Texture,
		buffers: &Vec<gltf::buffer::Data>,
	) -> Self {
		// glTF stores all binary data in base64 encoded blobs. Decoding textures is a little bit
		// annoying.
		log::info!("Loading texture from gltf...");
		let img_gltf = texture.source();
		let img_source = match img_gltf.source() {
			gltf::image::Source::View {
				view,
				mime_type,
			} => {
				// Buffers is list of buffers, view index is number in that list, buffer::Data
				// doesn't have named fields so the Vec<u8> that holds the data has to be grabed
				// with tuple notation.
				let buffer_data = &buffers[view.buffer().index()].0;
				let start = view.offset();
				let end = start + view.length();
				let data = &buffer_data[start..end];
				// I'm not interested in jpeg textures, and other formats are a hassle
				match mime_type {
					// "image/png" => image::load_from_memory_with_format(data, Png),
					// _ => panic!("Could not load image"),
					_ => image::load_from_memory(data),
				}
			},
			gltf::image::Source::Uri {
				uri,
				mime_type,
			} => {
				if uri.starts_with("data:") {
					let encoded = uri.split(',').nth(1).unwrap();
					let data = base64::decode(&encoded).unwrap();
					let mime_type = if let Some(ty) = mime_type {
						ty
					} else {
						uri.split(',')
							.nth(0)
							.unwrap()
							.split(':')
							.nth(1)
							.unwrap()
							.split(';')
							.nth(0)
							.unwrap()
					};

					match mime_type {
						// "image/png" => image::load_from_memory_with_format(&data, Png),
						// _ => panic!("Could not load image"),
						_ => image::load_from_memory(&data),
					}
				} else {
					panic!("I'm not dealing with loading from filesystem")
				}
			},
		};

		let img = img_source.unwrap();
		let (width, height) = img.dimensions();
		let rgba = img.into_rgba8().into_raw();

		let size = wgpu::Extent3d {
			width,
			height,
			depth: 1,
		};

		let texture = device.create_texture(&wgpu::TextureDescriptor {
			size,
			mip_level_count: 1,
			sample_count: 1,
			dimension: wgpu::TextureDimension::D2,
			format: wgpu::TextureFormat::Rgba8UnormSrgb,
			// SAMPLED = used in shaders
			// COPY_DST = copy data to texture
			usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
			label: Some("Diffuse Texture"),
		});

		// A "TextureView" is a window that lets the GPU look at the texture. The "Sampler"
		// defines how data is picked out of the texture.
		let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
		let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
			address_mode_u: wgpu::AddressMode::ClampToEdge,
			address_mode_v: wgpu::AddressMode::ClampToEdge,
			address_mode_w: wgpu::AddressMode::ClampToEdge,
			mag_filter: wgpu::FilterMode::Linear,
			min_filter: wgpu::FilterMode::Nearest,
			mipmap_filter: wgpu::FilterMode::Nearest,
			..Default::default()
		});

		queue.write_texture(
			wgpu::TextureCopyView {
				texture:   &texture,
				mip_level: 0,
				origin:    wgpu::Origin3d::ZERO,
			},
			rgba.as_slice(),
			wgpu::TextureDataLayout {
				offset:         0,
				bytes_per_row:  4 * width,
				rows_per_image: height,
			},
			size,
		);

		log::info!("Done");

		Self {
			sampler,
			texture,
			view,
		}
	}
}

impl BindGroup for Material {
	fn bind_group_layout(&self, device: &wgpu::Device) -> wgpu::BindGroupLayout {
		device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			entries: &[
				wgpu::BindGroupLayoutEntry {
					binding:    0,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Texture {
						multisampled:   false,
						view_dimension: wgpu::TextureViewDimension::D2,
						sample_type:    wgpu::TextureSampleType::Float {
							filterable: false
						},
					},
					count:      None,
				},
				wgpu::BindGroupLayoutEntry {
					binding:    1,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Sampler {
						comparison: false,
						filtering:  true,
					},
					count:      None,
				},
			],
			label:   Some("texture bind group layout"),
		})
	}

	fn bind_group(
		&self,
		device: &wgpu::Device,
		layout: &wgpu::BindGroupLayout,
		buffer: Option<&wgpu::Buffer>,
	) -> wgpu::BindGroup {
		device.create_bind_group(&wgpu::BindGroupDescriptor {
			layout,
			entries: &[
				wgpu::BindGroupEntry {
					binding:  0,
					resource: wgpu::BindingResource::TextureView(&self.view),
				},
				wgpu::BindGroupEntry {
					binding:  1,
					resource: wgpu::BindingResource::Sampler(&self.sampler),
				},
			],
			label: Some("texture bind group"),
		})
	}
}

crate struct Pbr {
	crate diffuse:      Material,
	crate displacement: Material,
	crate mr:           Material, // metallic/roughness
	crate normal:       Material,
}

impl BindGroup for Pbr {
	fn bind_group_layout(&self, device: &wgpu::Device) -> wgpu::BindGroupLayout {
		device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			entries: &[
				// diffuse
				wgpu::BindGroupLayoutEntry {
					binding:    0,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Texture {
						multisampled:   false,
						view_dimension: wgpu::TextureViewDimension::D2,
						sample_type:    wgpu::TextureSampleType::Float {
							filterable: false
						},
					},
					count:      None,
				},
				wgpu::BindGroupLayoutEntry {
					binding:    1,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Sampler {
						comparison: false,
						filtering:  true,
					},
					count:      None,
				},
				// displacement
				wgpu::BindGroupLayoutEntry {
					binding:    2,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Texture {
						multisampled:   false,
						view_dimension: wgpu::TextureViewDimension::D2,
						sample_type:    wgpu::TextureSampleType::Float {
							filterable: false
						},
					},
					count:      None,
				},
				wgpu::BindGroupLayoutEntry {
					binding:    3,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Sampler {
						comparison: false,
						filtering:  true,
					},
					count:      None,
				},
				// metallic/roughness
				wgpu::BindGroupLayoutEntry {
					binding:    4,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Texture {
						multisampled:   false,
						view_dimension: wgpu::TextureViewDimension::D2,
						sample_type:    wgpu::TextureSampleType::Float {
							filterable: false
						},
					},
					count:      None,
				},
				wgpu::BindGroupLayoutEntry {
					binding:    5,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Sampler {
						comparison: false,
						filtering:  true,
					},
					count:      None,
				},
				// normal
				wgpu::BindGroupLayoutEntry {
					binding:    6,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Texture {
						multisampled:   false,
						view_dimension: wgpu::TextureViewDimension::D2,
						sample_type:    wgpu::TextureSampleType::Float {
							filterable: false
						},
					},
					count:      None,
				},
				wgpu::BindGroupLayoutEntry {
					binding:    7,
					visibility: wgpu::ShaderStage::FRAGMENT,
					ty:         wgpu::BindingType::Sampler {
						comparison: false,
						filtering:  true,
					},
					count:      None,
				},
			],
			label:   Some("pbr material bind group layout"),
		})
	}

	fn bind_group(
		&self,
		device: &wgpu::Device,
		layout: &wgpu::BindGroupLayout,
		buffer: Option<&wgpu::Buffer>,
	) -> wgpu::BindGroup {
		device.create_bind_group(&wgpu::BindGroupDescriptor {
			layout,
			entries: &[
				wgpu::BindGroupEntry {
					binding:  0,
					resource: wgpu::BindingResource::TextureView(&self.diffuse.view),
				},
				wgpu::BindGroupEntry {
					binding:  1,
					resource: wgpu::BindingResource::Sampler(&self.diffuse.sampler),
				},
				wgpu::BindGroupEntry {
					binding:  2,
					resource: wgpu::BindingResource::TextureView(&self.displacement.view),
				},
				wgpu::BindGroupEntry {
					binding:  3,
					resource: wgpu::BindingResource::Sampler(&self.displacement.sampler),
				},
				wgpu::BindGroupEntry {
					binding:  4,
					resource: wgpu::BindingResource::TextureView(&self.mr.view),
				},
				wgpu::BindGroupEntry {
					binding:  5,
					resource: wgpu::BindingResource::Sampler(&self.mr.sampler),
				},
				wgpu::BindGroupEntry {
					binding:  6,
					resource: wgpu::BindingResource::TextureView(&self.normal.view),
				},
				wgpu::BindGroupEntry {
					binding:  7,
					resource: wgpu::BindingResource::Sampler(&self.normal.sampler),
				},
			],
			label: Some("pbr material bind group"),
		})
	}
}
