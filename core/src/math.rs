#![feature(associated_type_bounds)]
// TODO: Build this into a linear alg lib. Replace cgmath
mod point2;
mod point3;
mod point4;

mod vec2;
mod vec3;
mod vec4;

mod mat2;
mod mat3;
mod mat4;

crate use mat2::Mat2;
crate use mat3::Mat3;
crate use mat4::Mat4;
crate use point2::Point2;
crate use point3::Point3;
crate use point4::Point4;
crate use vec2::Vec2;
crate use vec3::Vec3;
crate use vec4::Vec4;
