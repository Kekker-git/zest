crate struct Mat2<T: std::ops::Add> {
	x: T,
	y: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Mat2<T>> for Mat2<T> {
	type Output = Mat2<T>;

	fn add(self, rhs: Mat2<T>) -> Self::Output {
		Mat2 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
		}
	}
}
