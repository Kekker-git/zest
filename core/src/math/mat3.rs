crate struct Mat3<T: std::ops::Add> {
	x: T,
	y: T,
	z: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Mat3<T>> for Mat3<T> {
	type Output = Mat3<T>;

	fn add(self, rhs: Mat3<T>) -> Self::Output {
		Mat3 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
			z: self.z + rhs.z,
		}
	}
}
