crate struct Mat4<T: std::ops::Add> {
	x: T,
	y: T,
	z: T,
	w: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Mat4<T>> for Mat4<T> {
	type Output = Mat4<T>;

	fn add(self, rhs: Mat4<T>) -> Self::Output {
		Mat4 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
			z: self.z + rhs.z,
			w: self.w + rhs.w,
		}
	}
}
