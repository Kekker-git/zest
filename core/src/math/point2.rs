crate struct Point2<T: std::ops::Add> {
	x: T,
	y: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Point2<T>> for Point2<T> {
	type Output = Point2<T>;

	fn add(self, rhs: Point2<T>) -> Self::Output {
		Point2 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
		}
	}
}
