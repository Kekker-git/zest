crate struct Point3<T: std::ops::Add> {
	x: T,
	y: T,
	z: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Point3<T>> for Point3<T> {
	type Output = Point3<T>;

	fn add(self, rhs: Point3<T>) -> Self::Output {
		Point3 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
			z: self.z + rhs.z,
		}
	}
}
