crate struct Point4<T: std::ops::Add> {
	x: T,
	y: T,
	z: T,
	w: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Point4<T>> for Point4<T> {
	type Output = Point4<T>;

	fn add(self, rhs: Point4<T>) -> Self::Output {
		Point4 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
			z: self.z + rhs.z,
			w: self.w + rhs.w,
		}
	}
}
