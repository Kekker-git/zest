crate struct Vec2<T: std::ops::Add> {
	x: T,
	y: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Vec2<T>> for Vec2<T> {
	type Output = Vec2<T>;

	fn add(self, rhs: Vec2<T>) -> Self::Output {
		Vec2 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
		}
	}
}
