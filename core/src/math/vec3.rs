crate struct Vec3<T: std::ops::Add> {
	x: T,
	y: T,
	z: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Vec3<T>> for Vec3<T> {
	type Output = Vec3<T>;

	fn add(self, rhs: Vec3<T>) -> Self::Output {
		Vec3 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
			z: self.z + rhs.z,
		}
	}
}
