crate struct Vec4<T: std::ops::Add> {
	x: T,
	y: T,
	z: T,
	w: T,
}

impl<T: std::ops::Add + std::ops::Add<Output = T>> std::ops::Add<Vec4<T>> for Vec4<T> {
	type Output = Vec4<T>;

	fn add(self, rhs: Vec4<T>) -> Self::Output {
		Vec4 {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
			z: self.z + rhs.z,
			w: self.w + rhs.w,
		}
	}
}
