use wgpu::util::DeviceExt;

use crate::{
	material::Material,
	vertex::Vertex,
};

crate struct ModelData {
	crate vertex_buffer:     wgpu::Buffer,
	crate index_buffer:      wgpu::Buffer,
	crate index_buffer_size: u32,
	crate material:          Material,
}

impl ModelData {
	crate fn new(render: &crate::render::RenderState, file: &str) -> Result<Self, gltf::Error> {
		let device = &render.device;
		let queue = &render.queue;
		// Note: This is really slow in debug. Still kinda slow in release but not terrible.
		log::info!("Creating glTF import data...");
		// I feel like 'images' is useful. It should probably be passed into
		// buffer::Texture::from_gltf()
		let (doc, buffers, images) = gltf::import(file)?;
		log::info!("Done.");

		log::info!("Building model data...");
		// This is all glTF stuff. Basically, glTF works by having a bunch of arrays that index into
		// other arrays that eventually index into a binary blob. It's not too complicated until you
		// have to parse the binary blob. That gets annoying. See crate::model_data::load_texture
		// for an example of annoyingness.
		let mesh = doc.meshes().next().unwrap();
		let primitive = mesh.primitives().next().unwrap();
		let material = primitive.material();

		let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));

		let mut vbuf = Vec::new();
		let mut ibuf = Vec::new();
		let mut index_buffer_size = 0;

		if let Some(pos_iter) = reader.read_positions() {
			if let Some(norm_iter) = reader.read_normals() {
				if let Some(tex_iter) = reader.read_tex_coords(0) {
					let tex_iter = match tex_iter {
						gltf::mesh::util::ReadTexCoords::F32(i) => i,
						_ => panic!("Texture coords not floats"),
					};
					for ((pos, norm), tex) in pos_iter.zip(norm_iter).zip(tex_iter) {
						let vertex = Vertex {
							pos,
							norm,
							tex,
						};
						vbuf.push(vertex);
					}
				}
			}
		}

		if let Some(iter) = reader.read_indices() {
			let iter = iter.into_u32();
			index_buffer_size = iter.len() as u32;
			for index in iter {
				ibuf.push(index)
			}
		}

		let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label:    Some("Vertex Buffer"),
			contents: bytemuck::cast_slice(vbuf.as_slice()),
			usage:    wgpu::BufferUsage::VERTEX,
		});
		let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label:    Some("Index Buffer"),
			contents: bytemuck::cast_slice(ibuf.as_slice()),
			usage:    wgpu::BufferUsage::INDEX,
		});

		let pbr = material.pbr_metallic_roughness();

		let normal = material.normal_texture();
		// mr = metallic/roughness
		// glTF doesn't support displacement maps directly. Maybe override the occlusion map with
		// disp, and bake occlusion into diffuse?
		// glTF puts metalness in the B channel, roughness in the G channel. Can probably slap
		// displacement into the R channel if I wanted to.
		let (diffuse, mr) = (pbr.base_color_texture(), pbr.metallic_roughness_texture());

		let material = if let Some(m) = diffuse {
			Material::from_gltf(device, queue, m.texture(), &buffers)
		} else if let Some(m) = normal {
			Material::from_gltf(device, queue, m.texture(), &buffers)
		} else if let Some(m) = mr {
			Material::from_gltf(device, queue, m.texture(), &buffers)
		} else {
			panic!("Bruh this model has no textures, what's up with that")
		};

		log::info!("Done.");

		Ok(Self {
			vertex_buffer,
			index_buffer,
			index_buffer_size,
			material,
		})
	}
}
