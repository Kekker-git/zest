crate mod bind_group;
crate mod depth;

use crate::vertex::Vertex;

// TODO: Make this more robust. Need to be able to define
// x shaders
// - bind group layouts
// - buffer layouts
// x labels
// - PrimitiveState args
// - etc
crate struct Node {
	crate label:              String,
	// shaders
	vs_module:                wgpu::ShaderModule,
	fs_module:                wgpu::ShaderModule,
	// layouts
	crate bind_group_layouts: Vec<wgpu::BindGroupLayout>,
	crate bind_groups:        hashbrown::HashMap<String, wgpu::BindGroup>,
	crate buffers:            hashbrown::HashMap<String, wgpu::Buffer>,
	pipeline_layout:          wgpu::PipelineLayout,
	crate pipeline:           wgpu::RenderPipeline,
}

impl Node {
	crate fn new(
		label: &str,
		device: &wgpu::Device,
		format: wgpu::TextureFormat,
		vert: Vec<u8>,
		frag: Vec<u8>,
		layouts: Vec<wgpu::BindGroupLayout>,
	) -> Self {
		// let mut v: Vec<&wgpu::BindGroupLayout> = Vec::new();
		// for l in &layouts {
		// 	v.push(&l)
		// }
		let v = &layouts.iter().collect::<Vec<&wgpu::BindGroupLayout>>();
		let vs_module = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
			label:  Some(&make_label(label, "vertex shader module")),
			source: wgpu::util::make_spirv(&vert),
			flags:  wgpu::ShaderFlags::VALIDATION,
		});
		let fs_module = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
			label:  Some(&make_label(label, "fragment shader module")),
			source: wgpu::util::make_spirv(&frag),
			flags:  wgpu::ShaderFlags::VALIDATION,
		});

		let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
			label:                Some(&make_label(label, "pipline layout")),
			bind_group_layouts:   &v.as_slice(),
			push_constant_ranges: &[],
		});

		let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
			label:         Some(&make_label(&label, "pipeline")),
			layout:        Some(&pipeline_layout),
			vertex:        wgpu::VertexState {
				module:      &vs_module,
				entry_point: "main",
				// This is the *layout* of the buffers used, not the buffers themselves. Should be
				// called "buffer_layouts" or "buffer_descriptors", but it isn't.
				buffers:     &[Vertex::layout()],
			},
			fragment:      Some(wgpu::FragmentState {
				module:      &fs_module,
				entry_point: "main",
				targets:     &[wgpu::ColorTargetState {
					format,
					alpha_blend: wgpu::BlendState::REPLACE,
					color_blend: wgpu::BlendState::REPLACE,
					write_mask: wgpu::ColorWrite::ALL,
				}],
			}),
			primitive:     wgpu::PrimitiveState {
				topology:           wgpu::PrimitiveTopology::TriangleList,
				strip_index_format: None,
				// CCW (counterclockwise) is convention. Can realistically be the other way around.
				front_face:         wgpu::FrontFace::Ccw,
				cull_mode:          wgpu::CullMode::Back,
				// Anything else requires Features::NON_FILL_POLYGON_MODE
				polygon_mode:       wgpu::PolygonMode::Fill,
			},
			depth_stencil: None,
			multisample:   wgpu::MultisampleState {
				count: 1,
				mask: !0,
				alpha_to_coverage_enabled: false,
			},
		});

		Self {
			label: label.to_string(),
			vs_module,
			fs_module,
			bind_group_layouts: layouts,
			bind_groups: hashbrown::HashMap::new(),
			buffers: hashbrown::HashMap::new(),
			pipeline_layout,
			pipeline,
		}
	}

	crate fn add_bind_group(
		&mut self,
		label: &str,
		bind_group: wgpu::BindGroup,
		buffer: Option<wgpu::Buffer>,
	) {
		self.bind_groups.insert(label.to_string(), bind_group);
		if let Some(b) = buffer {
			self.buffers.insert(label.to_string(), b);
		}
	}

	crate fn rebuild(
		&self,
		device: &wgpu::Device,
		format: wgpu::TextureFormat,
	) -> wgpu::RenderPipeline {
		device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
			label:         Some(&make_label(&self.label, "pipeline")),
			layout:        Some(&self.pipeline_layout),
			vertex:        wgpu::VertexState {
				module:      &self.vs_module,
				entry_point: "main",
				// This is the *layout* of the buffers used, not the buffers themselves. Should be
				// called "buffer_layouts" or "buffer_descriptors", but it isn't.
				buffers:     &[Vertex::layout()],
			},
			fragment:      Some(wgpu::FragmentState {
				module:      &self.fs_module,
				entry_point: "main",
				targets:     &[wgpu::ColorTargetState {
					format,
					alpha_blend: wgpu::BlendState::REPLACE,
					color_blend: wgpu::BlendState::REPLACE,
					write_mask: wgpu::ColorWrite::ALL,
				}],
			}),
			primitive:     wgpu::PrimitiveState {
				topology:           wgpu::PrimitiveTopology::TriangleList,
				strip_index_format: None,
				// CCW (counterclockwise) is convention. Can realistically be the other way around.
				front_face:         wgpu::FrontFace::Ccw,
				cull_mode:          wgpu::CullMode::Back,
				// Anything else requires Features::NON_FILL_POLYGON_MODE
				polygon_mode:       wgpu::PolygonMode::Fill,
			},
			depth_stencil: None,
			multisample:   wgpu::MultisampleState {
				count: 1,
				mask: !0,
				alpha_to_coverage_enabled: false,
			},
		})
	}
}

fn make_label(label: &str, ext: &str) -> String {
	format!("{} {}", label, ext)
}
