// Bind groups are how resources get passed to a shader. This trait allows any data structure to
// create a bind group.
// Bind groups are what GLSL would refer to as a "set", literally a set of bindings.
crate trait BindGroup {
	fn bind_group(
		&self,
		device: &wgpu::Device,
		layout: &wgpu::BindGroupLayout,
		buffer: Option<&wgpu::Buffer>,
	) -> wgpu::BindGroup;
	fn bind_group_layout(&self, device: &wgpu::Device) -> wgpu::BindGroupLayout;
}
