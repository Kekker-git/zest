crate struct Depth;

impl Depth {
	crate const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

	crate fn create_depth_texture(device: &wgpu::Device, swapchain_descriptor: &wgpu::SwapChainDescriptor, label: &str) {
		let size = wgpu::Extent3d {
			width: swapchain_descriptor.width,
			height: swapchain_descriptor.height,
			depth: 1,
		};
	}
}
