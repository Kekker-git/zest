use winit::{
	event::*,
	window::Window,
};

use crate::{
	camera::{
		Camera,
		CameraController,
	},
	model_data::ModelData,
	node::{
		bind_group::BindGroup,
		Node,
	},
};

// Basic overview of the components of a renderer:
// You create a window as your drawing surface. You then query for a physical adapter (gpu), which
// is then abstracted into a "device". Images are rendered by passing commands to a command buffer,
// which encodes the commands and sends them to a device, which produces an image. The image is sent
// to a queue, which put on the swapchain when it is ready, which then swaps the image on the
// surface with the new image on the swapchain. Magic.
//
// Pipelines describe exactly what the gpu does when the command buffer sends it stuff to do. Pretty
// much the entire renderer is boilerplate code, and the only reason it would ever need to be
// changed is if I was targeting more specific setups, such as multiple render surfaces (which would
// require multiple swapchains) or multi-GPU setups/optimizations. The bulk of the GPU programming
// and optimization is going to be done in the pipeline section over in pipeline.rs.

// Unfortunately, everything is synchronous right now while I work on getting what essentially
// amounts to boilerplate written. I'll have to see what can be async when I'm done with everything.
// I'm thinking probably gltf loading should be async? Who knows.
crate struct RenderState {
	// render stuff
	crate surface:              wgpu::Surface,
	crate device:               wgpu::Device,
	crate queue:                wgpu::Queue,
	crate swapchain_descriptor: wgpu::SwapChainDescriptor,
	crate swapchain:            wgpu::SwapChain,
	crate size:                 winit::dpi::PhysicalSize<u32>,
	// crate nodes:                hashbrown::HashMap<String, (wgpu::RenderPipeline,
	// PipelineData)>, crate bind_groups:          hashbrown::HashMap<String, wgpu::BindGroup>,
	clear_color:                wgpu::Color,
}

impl RenderState {
	crate async fn new(window: &Window) -> Self {
		log::info!("Initializing renderer...");
		let size = window.inner_size();

		// API instance
		let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
		// Window to draw to
		let surface = unsafe { instance.create_surface(window) };
		// Adapter and device are different abstractions of the physical GPU. Adapter is the GPU
		// itself, device is the software representation of it.
		let adapter = instance
			.request_adapter(&wgpu::RequestAdapterOptions {
				power_preference:   wgpu::PowerPreference::default(),
				compatible_surface: Some(&surface),
			})
			.await
			.unwrap();

		// Not sre what the queue is exactly... queue of rendered images?
		let (device, queue) = adapter
			.request_device(
				&wgpu::DeviceDescriptor {
					features: wgpu::Features::empty(),
					limits:   wgpu::Limits::default(),
					label:    None,
				},
				None,
			)
			.await
			.unwrap();

		// Pretty self explanatory. Describes this specific swapchain.
		// Generally, you only use one swapchain (I think). Multiple render passes are handled with
		// pipelines.
		// usage: RENDER_ATTACHMENT is the final presentation. Other usages can probably be used to
		// chain swapchains together, but that's probably best left to pipelines.
		let swapchain_descriptor = wgpu::SwapChainDescriptor {
			usage:        wgpu::TextureUsage::RENDER_ATTACHMENT,
			format:       adapter.get_swap_chain_preferred_format(&surface),
			width:        size.width,
			height:       size.height,
			// Mailbox is true triple buffering, where the newest frame is rendered to the oldest
			// buffer, and the newest complete render is sent to the GPU.
			// Immediate is double buffering with no vertical sync. Screen tearing galore.
			// FIFO is vsync. Input lag galore.
			// In game, present the option as "Reduce tearing <o >". Don't include a vsync option,
			// vsync sucks. Do, however, include a description that specifies what it does.
			present_mode: wgpu::PresentMode::Mailbox,
		};
		let swapchain = device.create_swap_chain(&surface, &swapchain_descriptor);

		let clear_color = wgpu::Color {
			r: 0.1,
			g: 0.2,
			b: 0.3,
			a: 1.0,
		};

		log::info!("Done.");
		RenderState {
			surface,
			device,
			queue,
			swapchain_descriptor,
			swapchain,
			size,

			clear_color,
		}
	}

	// pub fn add_pipeline(&mut self, pipeline_data: crate::pipeline::PipelineData) {
	// 	let pipeline = pipeline_data.build(&self.device, self.swapchain_descriptor.format);
	// 	self.pipelines
	// 		.insert(pipeline_data.label.to_string(), (pipeline, pipeline_data));
	// }

	crate fn input(&mut self, event: &WindowEvent, controller: &mut CameraController) -> bool {
		match event {
			WindowEvent::CursorMoved {
				position, ..
			} => {
				self.clear_color = wgpu::Color {
					r: position.x as f64 / self.size.width as f64,
					g: position.y as f64 / self.size.height as f64,
					b: 1.0,
					a: 1.0,
				};
				true
			},
			WindowEvent::KeyboardInput {
				input, ..
			} => {
				// Originally this section created a move vector every frame and modified it based
				// on letter input. This caused a mind boggling problem where the camera would jerk,
				// then stop, then move with a stutter if a key was held down.
				// It took a literal week to realize that this was happening bc this method relies
				// on the keyboard to send repeat letters. You know how when you hold down a key, it
				// types the letter, then waits, then repeats? Yea, that was happening but for
				// motion. So... if you run into this problem, make a camera controller that changes
				// state based on input. Don't create a vector that depends on that frame's input.
				match input {
					KeyboardInput {
						state,
						virtual_keycode: Some(VirtualKeyCode::W),
						..
					} => {
						controller.fore = match state {
							ElementState::Pressed => true,
							ElementState::Released => false,
						}
					},
					KeyboardInput {
						state,
						virtual_keycode: Some(VirtualKeyCode::S),
						..
					} => {
						controller.back = match state {
							ElementState::Pressed => true,
							ElementState::Released => false,
						}
					},
					KeyboardInput {
						state,
						virtual_keycode: Some(VirtualKeyCode::A),
						..
					} => {
						controller.left = match state {
							ElementState::Pressed => true,
							ElementState::Released => false,
						}
					},
					KeyboardInput {
						state,
						virtual_keycode: Some(VirtualKeyCode::D),
						..
					} => {
						controller.right = match state {
							ElementState::Pressed => true,
							ElementState::Released => false,
						}
					},
					_ => return false,
				}
				true
			},
			_ => {
				self.clear_color = wgpu::Color {
					r: 0.1,
					g: 0.2,
					b: 0.3,
					a: 1.0,
				};
				false
			},
		}
	}

	crate fn render(
		&mut self,
		model_data: &ModelData,
		node: &crate::node::Node,
	) -> Result<(), wgpu::SwapChainError> {
		// Contains texture that we will write to. Texture is the image the swapchain previously
		// presented.
		let frame = self.swapchain.get_current_frame()?.output;

		// Builds a command buffer
		let mut encoder = self
			.device
			.create_command_encoder(&wgpu::CommandEncoderDescriptor {
				label: Some("Render Encoder"),
			});

		// Closed scope allows us to close off the mutable encoder borrow in _render_pass before
		// submitting it. Can also use drop(_render_pass) to be more explicit; this is ultimately
		// what's done here. Note that the pipeline and vertex buffer have to be defined before the
		// pass so that they are dropped in the correct order.
		// {
		let mut pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
			label:                    Some("Render pass"),
			color_attachments:        &[wgpu::RenderPassColorAttachmentDescriptor {
				attachment:     &frame.view,
				resolve_target: None,
				ops:            wgpu::Operations {
					load:  wgpu::LoadOp::Clear(self.clear_color),
					store: true,
				},
			}],
			depth_stencil_attachment: None,
		});

		pass.set_pipeline(&node.pipeline);
		for (i, (_, bg)) in node.bind_groups.iter().enumerate() {
			// i is the "set" in GLSL. Not sure what the offset (the empty slice) is used for.
			pass.set_bind_group(i as u32, &bg, &[])
		}
		// This is where vertex buffers are actually set. The pipelines only set the buffer layouts,
		// not the buffers themselves. This allows for easy swapping on the fly without having to
		// rebuild pipelines.
		pass.set_vertex_buffer(0, model_data.vertex_buffer.slice(..));
		pass.set_index_buffer(model_data.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
		// This defines gl_VertexIndex. vextex_num vertices (0-vertex_num) and 1 instance (0).
		pass.draw_indexed(0..model_data.index_buffer_size, 0, 0..1);
		// This is where we'd close the scope if we didn't manually drop(pass)
		// }
		drop(pass);

		self.queue.submit(std::iter::once(encoder.finish()));

		Ok(())
	}

	crate fn resize(&mut self, resized: winit::dpi::PhysicalSize<u32>) {
		self.size = resized;
		self.swapchain_descriptor.width = resized.width;
		self.swapchain_descriptor.height = resized.height;
		self.swapchain = self
			.device
			.create_swap_chain(&self.surface, &self.swapchain_descriptor);
	}

	crate fn update<T: bytemuck::Pod>(&mut self, buffer: &wgpu::Buffer, data: T) {
		// This is how uniforms get updated. The *buffer* gets overwritten, but the *bind group*
		// does not change. That is important. Rebuilding bind groups, pipelines, etc. is costly.
		// Don't do it.
		self.queue
			.write_buffer(buffer, 0, bytemuck::cast_slice(&[data]))
	}
}
