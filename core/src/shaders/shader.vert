#version 450

layout(location=0) in vec3 a_pos;
layout(location=1) in vec3 a_norm;
layout(location=2) in vec2 a_tex;

layout(location=0) out vec2 v_tex;

layout(set = 1, binding = 0) uniform Camera {
	mat4 u_view_proj;
};

void main() {
	// vec3 view = vec3(gl_ModelViewMatrix * gl_Vertex);
	// vec3 norm = vec3(gl_NormalMatrix * gl_Normal);
	v_tex = a_tex;
	gl_Position = u_view_proj * vec4(a_pos, 1.0);
}
