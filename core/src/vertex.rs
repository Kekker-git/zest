#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
crate struct Vertex {
	crate pos:  [f32; 3],
	// col: [f32; 3],
	crate norm: [f32; 3],
	crate tex:  [f32; 2],
}

// These make it way easier to reason about attribute offsets. They're not in
// the impl bc I don't want duplicate constants for every vertex, that's a waste
// of memory.
const POS_SIZE: usize = std::mem::size_of::<[f32; 3]>();
const NORM_SIZE: usize = std::mem::size_of::<[f32; 3]>();
const TEX_SIZE: usize = std::mem::size_of::<[f32; 2]>();

impl Vertex {
	crate fn layout<'a>() -> wgpu::VertexBufferLayout<'a> {
		wgpu::VertexBufferLayout {
			array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
			step_mode:    wgpu::InputStepMode::Vertex,
			attributes:   &[
				// Positions
				wgpu::VertexAttribute {
					offset:          0,
					shader_location: 0,
					format:          wgpu::VertexFormat::Float3,
				},
				// Normals
				wgpu::VertexAttribute {
					// 1x format size. Float3 = 3 floats = [f32; 3]. A third attribute would be at
					// offset size_of::<[f32; 6]>. Alternatively (and perhaps more intuitively),
					// size_of::<[f32; 3]> + size_of::<[f32; 3]>
					offset:          POS_SIZE as wgpu::BufferAddress,
					shader_location: 1,
					format:          wgpu::VertexFormat::Float3,
				},
				// Texture coordinates
				wgpu::VertexAttribute {
					offset:          (POS_SIZE + NORM_SIZE) as wgpu::BufferAddress,
					shader_location: 2,
					format:          wgpu::VertexFormat::Float2,
				},
			],
		}
	}
}
